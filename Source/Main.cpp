/*
  ==============================================================================

    This file contains the basic startup code for a JUCE application.

  ==============================================================================
*/

#include <JuceHeader.h>

#include "ursa_stable_sweep.h"

//==============================================================================
int main (int argc, char* argv[])
{
    StableSweep sweep(juce::File::getCurrentWorkingDirectory().getChildFile("out").getChildFile("stableSweep.wav"),44100.00);
   
    sweep.process();

    return 0;
}
