/*
  ==============================================================================

    ursa_stable_sweep.h
    Created: 10 Dec 2021 10:15:46am
    Author:  dave

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

class StableSweep 
{
public:
    StableSweep(juce::File destination, double sampleRate)
        : sampleRate(sampleRate), destination(destination)
    {
    }

    void process()
    {
        juce::AudioFormatManager mgr;
        mgr.registerBasicFormats();
        auto* wavFormat = mgr.findFormatForFileExtension(formatExtension);

        if(wavFormat==nullptr)
        {
            jassertfalse;
            return;
        }

        if(destination.exists())
        {
            if(!destination.moveFileTo(destination.getNonexistentSibling()))
            {
                jassertfalse;
                return;
            }
        }

        jassert(destination.create());

        auto stream = destination.createOutputStream();

        if(stream==nullptr ||stream->failedToOpen())
        {
            jassertfalse;
            return;
        }

        auto writer = std::unique_ptr<juce::AudioFormatWriter>(wavFormat->createWriterFor(stream.release(),sampleRate,1,24,{},0));

        const auto numSamples = juce::roundToInt((stableLen + sweepLen) * sampleRate);
        juce::AudioBuffer<float> buf{1,numSamples};


        auto* wptr = buf.getWritePointer(0);
        double sinePos = 0.0;

        for(int s=0;s<numSamples;s++)
        {
            const auto sd = double(s);
            const auto t = sd / sampleRate;

            if (t < stableLen)
            {
                sinePos = t * startFrequency * juce::MathConstants<double>::twoPi;
                wptr[s] = float(sin(sinePos));
            }
            else
            {
                const auto timeInSweep = (t - stableLen);
                // calc freq at this point
                const auto freq = ((endFrequency-startFrequency)/sweepLen)*timeInSweep+startFrequency;
                const auto radPerSample = (freq * juce::MathConstants<double>::twoPi) / sampleRate;
                sinePos += radPerSample;
                wptr[s] = float(sin(sinePos));
            }
            
        }
        buf.applyGain(juce::Decibels::decibelsToGain(gainDB));

        writer->writeFromAudioSampleBuffer(buf,0,buf.getNumSamples());
        

    }

private:
    double sampleRate{44100.00};

    double stableLen{ 1 };
    double sweepLen{ 6 };

    double startFrequency{ 20.0 };
    double endFrequency{ 16000.0 };
    double gainDB{-3.0 };

    juce::String formatExtension{"wav"};
    juce::File destination;
};