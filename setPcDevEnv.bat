@echo off

:: store where we are
SET "de_projectdir=%cd%"
SET "de_projectdrive=%~d0"

:: find the folder for visual studio - sets vsdir variable

if exist "%programfiles(x86)%\Microsoft Visual Studio\2019\Community" (
	echo Vs Community detected
	SET "vsdir=%programfiles(x86)%\Microsoft Visual Studio\2019\Community"
) else (
	if exist "%programfiles(x86)%\Microsoft Visual Studio\2019\Professional" (
		echo Vs Professional detected
		SET "vsdir=%programfiles(x86)%\Microsoft Visual Studio\2019\Professional"
	)else (
		if exist "%programfiles(x86)%\Microsoft Visual Studio\2019\Enterprise" (
			echo Vs enterprise detected	
			SET "vsdir=%programfiles(x86)%\Microsoft Visual Studio\2019\Enterprise"
		) else (
			echo "WARNING - No edition of Vs2019 was detected, is it installed?"
		)
	)
)


:: this replaces the call "c:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\bin\vcvars32.bat"
call "%vsdir%\Common7\Tools\VsDevCmd.bat"

:: restore the drive letter after VsDevCmd.bat sets it to C:
%de_projectdrive%
CD %de_projectdir%


@echo on